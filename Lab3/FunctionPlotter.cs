﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Lab3
{
    public partial class FunctionPlotter : UserControl
    {
        private TabulationSettings tabulationSettings = TabulationSettings.Default;
        private TabulatedFunction function;

        private void updateData()  // TODO: use single collection for both plots
        {
            string seriesName = function.ReadableName;

            chart.Series[seriesName].Points.Clear();
            dataGrid.Rows.Clear();

            var points = function.Tabulate(tabulationSettings);
            var ys = points.Select(p => p.Y);

            foreach (Point p in points)
            {
                dataGrid.Rows.Add(p.X, p.Y);
                chart.Series[seriesName].Points.AddXY(p.X, p.Y);
            }

            chartSettings.SetMin(ys.Min());
            chartSettings.SetMax(ys.Max());
        }

        public FunctionPlotter()
        {
            InitializeComponent();

            this.chartSettings.Apply(tabulationSettings);
            this.chartSettings.RegisterUpdateHandler((object sender, EventArgs args) =>
            {
                this.tabulationSettings = this.chartSettings.GetTabulationSettings();
                updateData();
            });

            dataGrid.Columns.Add("X", "X");
            dataGrid.Columns.Add("Y", "Y");
        }

        public void SetFunction(TabulatedFunction function)
        {
            this.function = function;

            chart.Series.Clear();
            chart.Series.Add(function.ReadableName);
            updateData();

            string seriesName = function.ReadableName;
            chart.Series[seriesName].XValueMember = "X";
            chart.Series[seriesName].YValueMembers = "Y";
            chart.Series[seriesName].ChartType = SeriesChartType.Line;
        }
    }
}
